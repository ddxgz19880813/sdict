package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	// "html/template"
	"io/ioutil"
	"log"
	"os"
	"time"
	// "net/http"
	"sort"
	"strconv"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// var WordTypes []string = []string{"basic", "expression", "verb", "noun", "adj", "adverb"}
var WordTypes []string

type Word struct {
	gorm.Model
	Se         string
	En         string
	Cn         string
	Note       string
	RecordType string
	Known      int `sql:"DEFAULT:0" json:"Known,string"`
	// Known int `json:"Known,string"`
}

type Backup struct {
	Name   string `json:"name"`
	// ext string
	// Known int `json:"Known,string"`
}

// type Tag struct {
//     gorm.Model
//     Name string
// }

// type WordTag struct {
//     gorm.Model
//
// }

// var db *gorm.DB
// var openerr error

type DictStore struct {
	db         *gorm.DB
	DBPath     string
	DBType     string
	DBFileName string
}

// gorm.DefaultTableNameHandler = func (db *gorm.DB, defaultTableName string) string  {
//     return "prefix_" + defaultTableName
// }
//
// func (w Word) TableName(db *gorm.DB) string {
//     if w.RecordType == "basic" {
//         return "most_basic_word"
//     } else if w.RecordType == "expression" {
//         return "expression"
//     } else {
//         return "word"
//     }
// }

func (w Word) String() string {
	return "<ID:" + strconv.Itoa(int(w.ID)) + " - Se:" + w.Se + " - Type:" + w.RecordType + " - Known:" + strconv.Itoa(w.Known) + ">"
}

func (dStore *DictStore) ImportCSV(csvFileName string) error {
	if err := dStore.Backup(); err != nil {
		return err
	} else {
		file, _ := ioutil.ReadFile(csvFileName)
		csvReader := csv.NewReader(strings.NewReader(string(file)))
		header, _ := csvReader.Read()
		if header[0] == "se" {
			log.Println("Read Header: ", header)
		} else {
			nw := Word{Se: header[0], En: header[1], Cn: header[2], Note: header[3], RecordType: header[4]}
			dStore.InsertWord(&nw)
		}
		if records, err := csvReader.ReadAll(); err != nil {
			log.Println("ReadAll csv error: ", err)
			return nil
		} else {

			// fmt.Println(records)
			for _, r := range records {
				fmt.Println(r)
				nw := Word{Se: r[0], En: r[1], Cn: r[2], Note: r[3], RecordType: r[4]}
				fmt.Println("nw: ", nw)
				if err := dStore.InsertWord(&nw); err != nil {
					// restore db
					log.Println("insert new word fail: ", err)
					// RestoreDB()
					// return err
				}
			}
		}
		return nil
	}
}

func RestoreDB() error {
	log.Println("Restoring db")
	// conf, _ := LoadConfig(CONFIG_FILE)
	conf := CONFIG
	bkPath := "./backups"
	files, _ := ioutil.ReadDir(bkPath + "/")
	times := make([]string, len(files))
	for _, f := range files {
		time := f.Name()[len(f.Name())-14:]
		times = append(times, time)
	}
	// log.Println(times)
	sort.Strings(times)
	// log.Println(times)
	latest := times[len(times)-1]

	rePath := bkPath + "/" + conf.DBFileName + "." + latest
	fmt.Println(rePath)
	srcDB := conf.DBPath + conf.DBFileName
	fmt.Println(srcDB)
	err := CopyFile(rePath, srcDB)
	if err != nil {
		log.Println("err when CopyFile: ", err)
		return err
	}
	// err := CopyFile(conf.path, "")
	return nil
}

func (dStore *DictStore) Backup() error {
	log.Println("Backing up db")
	bkPath := "./backups"
	if _, err := os.Stat(bkPath); os.IsNotExist(err) {
		err := os.MkdirAll(bkPath, 0740)
		if err != nil {
			log.Fatalln("Create file error: ", err)
		}
		log.Println("Created DB Backup path: ", bkPath)
		return err
	}
	bkPath = bkPath + "/" + dStore.DBFileName + "." + time.Now().Format("20060102150402")
	// fmt.Println(bkPath)
	srcDB := dStore.DBPath + dStore.DBFileName
	// fmt.Println(srcDB)
	err := CopyFile(srcDB, bkPath)
	if err != nil {
		log.Println("err when CopyFile: ", err)
		return err
	}
	// err := CopyFile(dStore.path, "")
	return nil
}


func (dStore *DictStore) NamedBackup(name string) error {
	log.Println("Backing up db with name of ", name)
	bkPath := "./backups"
	if _, err := os.Stat(bkPath); os.IsNotExist(err) {
		err := os.MkdirAll(bkPath, 0740)
		if err != nil {
			log.Fatalln("Create file error: ", err)
		}
		log.Println("Created DB Backup path: ", bkPath)
		return err
	}
	bkPath = bkPath + "/" + dStore.DBFileName + "." +
		 time.Now().Format("20060102150402") + "." + name
	// fmt.Println(bkPath)
	srcDB := dStore.DBPath + dStore.DBFileName
	// fmt.Println(srcDB)
	err := CopyFile(srcDB, bkPath)
	if err != nil {
		log.Println("err when CopyFile: ", err)
		return err
	}
	// err := CopyFile(dStore.path, "")
	return nil
}

func (dStore *DictStore) Close() error {
	log.Println("Closing db")
	return dStore.db.Close()
}

func OpenDB() (*DictStore, error) {

	// conf, _ := LoadConfig(CONFIG_FILE)
	conf := CONFIG
	WordTypes = conf.RecordTypes
	return GetDBByConfig(&conf)
}

//
// func OpenTestDB() (*DictStore, error) {
//
//     // conf, _ := LoadConfig(CONFIG_FILE_TEST)
//     conf := CONFIG_TEST
//     return GetDBByConfig(conf)
// }
//
func GetDBByConfig(conf *Config) (*DictStore, error) {

	dbtype := conf.DBType
	path := conf.DBPath
	dbfile := conf.DBFileName
	dbpathfile := path + dbfile
	db, err := gorm.Open(dbtype, dbpathfile)
	if err != nil {
		log.Println("OpenDB open db err: ", err)
		return nil, err
	}

	dStore := DictStore{db: db, DBPath: path, DBFileName: dbfile, DBType: dbtype}
	log.Println("Opened DB: ", dStore.DBType, " from: ", dStore.DBPath+dStore.DBFileName)
	db.AutoMigrate(&Word{})

	return &dStore, nil
}

func IsSearchToken(token string) bool {
	var tokens = []string{"se", "en", "cn", "note"}
	for _, t := range tokens {
		if t == token {
			return true
		}
	}
	return false
}

func IsWordType(token string) bool {
	for _, t := range WordTypes {
		if t == token {
			return true
		}
	}
	return false
}

func (dStore *DictStore) GetWordByPk(pk int) Word {
	db := dStore.db
	var word Word
	db.First(&word, pk)
	// if token != "" {
	//     // qs := fmt.Sprintf("%v = ?", token)
	//     // match := fmt.Sprintf("%%%v%%", token)
	//     log.Println("pk: ", pk)
	//     db.Where(qs, match).First(&word)
	// } else {
	//     db.First(&word)
	// }

	return word
}

func (dStore *DictStore) SearchWordByTypeWithKnown(wt []string, known int) []Word {
	db := dStore.db
	var word []Word
	if len(wt) != 0 {
		// qs := fmt.Sprintf("RecordType LIKE ?")
		// match := fmt.Sprintf("%%%v%%", *w)
		log.Println("to find Known: ", known, "RecordType: ", wt)
		db.Where("record_type in (?) AND known = ?", wt, known).Find(&word)
		// } else {
		//     db.Find(&word)
	}

	return word
}

func (dStore *DictStore) SearchWordByType(wt []string) []Word {
	db := dStore.db
	var word []Word
	if len(wt) != 0 {
		// qs := fmt.Sprintf("RecordType LIKE ?")
		// match := fmt.Sprintf("%%%v%%", *w)
		log.Println("to find RecordType: ", wt)
		db.Where("record_type in (?)", wt).Find(&word)
		// } else {
		//     db.Find(&word)
	}

	return word
}

/* Can search by search token, or can pass the w as nil for get all the words
 */
func (dStore *DictStore) SearchWord(token string, w *string) []Word {
	db := dStore.db
	var word []Word
	if w != nil {
		qs := fmt.Sprintf("%v LIKE ?", token)
		match := fmt.Sprintf("%%%v%%", *w)
		log.Println("qs: ", qs, "match: ", match)
		db.Where(qs, match).Find(&word)
	} else {
		db.Find(&word)
	}

	return word
}

func (dStore *DictStore) AllWordWithKnown(known int) []Word {
	db := dStore.db
	var word []Word
	// qs := fmt.Sprintf("RecordType LIKE ?")
	// match := fmt.Sprintf("%%%v%%", *w)
	log.Println("to find all known: ", known)
	db.Where("known = ?", known).Find(&word)
	// } else {
	//     db.Find(&word)

	return word
}

func (ds *DictStore) Update(wordId uint, wd *Word) error {
	db := ds.db

	if wd != nil {
		var wo Word
		db.First(&wo, wordId)
		log.Println("to update word: ", wo, "by word.ID: ", wordId, ", to word:", wd)
		db.Model(&wo).Updates(wd)
		if err := db.Model(&wo).Updates(wd).Error; err != nil {
			return err
		} 

		db.Model(&wo).Update("Known", wd.Known)
		if err := db.Model(&wo).Update("Known", wd.Known).Error; err != nil {
			return err
		} 

	} else {
		return errors.New("input Word wd is nil!")
	}
	return nil	
}

// func (ds *DictStore) Update(wordId uint, wd *map[string]interface{}) error {
// 	db := ds.db

// 	if wd != nil {
// 		var wo Word
// 		db.First(&wo, wordId)
// 		log.Println("to update word: ", wo, "by word.ID: ", wordId, ", to word:", wd)
// 		db.Model(&wo).Updates(wd)
// 		if err := db.Model(&wo).Updates(wd).Error; err != nil {
// 			return err
// 		} 

// 	} else {
// 		return errors.New("input Word wd is nil!")
// 	}
// 	return nil	
// }

/* Only unexsited Se word can be inserted, for the sake of avoiding duplicate.
For update existed word, using Update()
*/
func (dStore *DictStore) InsertWord(wd *Word) error {
	// exi := db.NewRecord(wd)
	// if exi != true {
	//     fmt.Println("err when db.Create word")
	// }
	db := dStore.db

	var match []Word
	db.Where("Se = ?", wd.Se).Find(&match)
	if len(match) != 0 {
		log.Println("match existed for ", wd)
		return errors.New("match existed")
	}

	db.Create(&wd)
	if exi := db.NewRecord(wd); exi != false {
		fmt.Println("err when db.Create word")
		return errors.New("word not created")
	}
	return nil

}

// func OldInsertWord(wd *Word) (string, error) {
//     // exi := db.NewRecord(wd)
//     // if exi != true {
//     //     fmt.Println("err when db.Create word")
//     // }
//     var match []Word
//     db.Where("Se = ?", wd.Se).Find(&match)
//     if len(match) != 0 {
//         log.Println("match existed for ", wd)
//         return "Match existed for this word", errors.New("match existed")
//     } else {
//
//         db.Create(&wd)
//         exi := db.NewRecord(wd)
//         if exi != false {
//             fmt.Println("err when db.Create word")
//             return "db.create may not succesed", errors.New("word not created")
//         }
//         return "InsertWord success", nil
//     }
// }

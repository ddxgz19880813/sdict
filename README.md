# A Simple Dictionary

## Functionalities
- Store data in local db
- sync data table to Google Drive
- menu provides different categories of words and sections
-

## TODO
- ~~db path~~
- ~~db backup~~
- ~~import csv~~
- ~~request different categories of words~~
- ~~add modification of words~~
- ~~remote backup~~
- import on web
- ~~more word types, can be addressed in conf~~ 
- etcd for conf
- ~~tag words as known and unknown, api with filter~~
- a goroutine for export files to Google Drive
- improve url routing
- add lookup based on note
- fix bug in Edit
- more pages

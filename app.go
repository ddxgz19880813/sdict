package main

import (
	"encoding/json"
	"net"
	"os"
	// "errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	// "github.com/jinzhu/gorm"
	// _ "github.com/jinzhu/gorm/dialects/sqlite"

	"github.com/gorilla/mux"
)

var logger *log.Logger

type Page struct {
	Title string
	Body  []byte
}

type RespJSON struct {
	Content  []Word
	Previous string
	Next     string
}

// func loadPage(title string) (*Page, error) {
//     filename := title + ".txt"
// }

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	t, _ := template.ParseFiles(tmpl + ".html")
	t.Execute(w, p)
}

func renderNestTemplate(w http.ResponseWriter, content string, p *Page) {
	// t, _ := template.ParseFiles(tmpl + ".html")
	ts, err := template.ParseFiles("templates/base.tmpl", "templates/"+content+".tmpl")
	if err != nil {
		logger.Println("err when ParseFiles: ", err)
	}
	if err := ts.ExecuteTemplate(w, "content", p); err != nil {
		logger.Println("err when ParseFiles: ", err)
	}
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path[:], "/")
	title := path[1]
	p := &Page{title, []byte("page body")}
	renderTemplate(w, "templates/index", p)
}

func frontPageHandler(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path[:], "/")
	title := path[1]
	p := &Page{title, []byte("page body")}
	renderNestTemplate(w, "front", p)
}

func managementPageHandler(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path[:], "/")
	title := path[1]
	p := &Page{title, []byte("page body")}
	renderNestTemplate(w, "management", p)
}

// func (ds *DictStore) wordHandlerOld(w http.ResponseWriter, r *http.Request) {
// 	// path := strings.Split(r.URL.Path[:], "/")
// 	// fmt.Println("splited path: ", path, " len: ", len(path))
// 	switch r.Method {
// 	case "GET":
// 		log.Println("in getting GET of word")
// 		// path := r.URL.Path[1:3]
// 		path := strings.Split(r.URL.Path[:], "/")
// 		// token := path[2]
// 		// word := path[3]
// 		log.Println("put path: ", path, " len: ", len(path))
// 		// rUrl, _ := url.Parse(r.URL.Path[:])
// 		// log.Println("req url ", rUrl)
// 		qMap, _ := url.ParseQuery(r.URL.RawQuery)
// 		log.Println("query map: ", qMap)

// 		var items []Word
// 		log.Println("len of path: ", len(path), path)
// 		if len(path) > 2 && path[2] == "" {

// 			token := "word"
// 			items = ds.SearchWord(token, nil)
// 			fmt.Println(items)
// 		} else if len(path) > 2 && IsWordType(path[2]) {
// 			wordType := path[2]
// 			items = ds.SearchWordByType(strings.Fields(wordType))
// 			log.Println(items)

// 		}
// 		headers := r.Header.Get("Accept")
// 		log.Println("header accept: ", headers)
// 		w.WriteHeader(http.StatusOK)
// 		w.Header().Set("Content-Type", "text")

// 		respJson := RespJSON{items, "pre", "next"}
// 		jsondata, err := json.Marshal(&respJson)
// 		if err != nil {
// 			log.Printf("json marshal error\n")
// 		}
// 		// fmt.Println(string(jsondata[:]))

// 		fmt.Fprintf(w, string(jsondata[:]))

// 	case "PUT":
// 		log.Println("in getting PUT of word")
// 		path := strings.Split(r.URL.Path[:], "/")
// 		log.Println("put path: ", path, " len: ", len(path))

// 		value, err := ioutil.ReadAll(r.Body)
// 		log.Println("put data: ", string(value[:]))
// 		if err != nil {
// 			log.Println("error when read post body:", err)
// 			w.WriteHeader(http.StatusBadRequest)
// 			fmt.Fprintf(w, "error when read post body ")
// 		}

// 		var wd Word
// 		if err := json.Unmarshal(value, &wd); err != nil {
// 			log.Println("err when Unmarshal json")
// 			// if err == UnmarshalTypeError && err.Value == "bool" {
// 			//
// 			//     if wd.Known == "true" || wd.Know == "True" {
// 			//         wd.Known = true
// 			//     }
// 			// }
// 		}
// 		log.Println("put word: ", wd)

// 		wordId, err := strconv.ParseUint(path[2], 10, 32)
// 		if err != nil {
// 			log.Println("err when Update, cannot part path[2] to uint as word id")
// 			fmt.Fprintf(w, "Update failed, cannot part path[2] to uint as word id")
// 		}

// 		if err := ds.Update(uint(wordId), &wd); err != nil {
// 			log.Println("err when Update")
// 			//TODO: handel different errors
// 			fmt.Fprintf(w, "Update failed")
// 		} else {
// 			w.WriteHeader(http.StatusOK)
// 			fmt.Fprintf(w, "Update Success")
// 		}
// 	case "POST":
// 		log.Println("in getting POST of word")
// 		value, err := ioutil.ReadAll(r.Body)
// 		fmt.Println(string(value[:]))
// 		if err != nil {
// 			log.Println("error when read post body:", err)
// 			w.WriteHeader(http.StatusBadRequest)
// 			fmt.Fprintf(w, "error when read post body ")
// 		}

// 		var wd Word
// 		if err := json.Unmarshal(value, &wd); err != nil {
// 			log.Println("err when Unmarshal json")
// 		}
// 		log.Println("the posted data: ", wd)
// 		if err := ds.InsertWord(&wd); err != nil {
// 			log.Println("err when InsertWord")
// 			//TODO: handel different errors
// 			fmt.Fprintf(w, "InsertWord failed")
// 		} else {
// 			w.WriteHeader(http.StatusCreated)
// 			fmt.Fprintf(w, "InsertWord Success")
// 		}
// 	}
// }

type WordComing struct {
	Se         string
	En         string
	Cn         string
	Note       string
	RecordType string
	Known      int `json:"Known"`
}

func (ds *DictStore) backupHandler(w http.ResponseWriter, r *http.Request) {
	// path := strings.Split(r.URL.Path[:], "/")
	// fmt.Println("splited path: ", path, " len: ", len(path))
	switch r.Method {
	case "GET":
		log.Println("in getting GET of word")
		fmt.Fprintf(w, "got backups")

	case "POST":
		log.Println("in getting POST of backups")
		value, err := ioutil.ReadAll(r.Body)
		fmt.Println(string(value[:]))
		if err != nil {
			log.Println("error when read post body:", err)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "error when read post body ")
			return
		}

		var bk Backup
		if err := json.Unmarshal(value, &bk); err != nil {
			log.Println("err when Unmarshal json")
			fmt.Fprintf(w, "NamedBackup failed, err when parse json!")
			return
		}
		log.Println("the posted data: ", bk)
		if err := ds.NamedBackup(bk.Name); err != nil {
			log.Println("err when NamedBackup")
			//TODO: handel different errors
			fmt.Fprintf(w, "NamedBackup failed")
		} else {
			w.WriteHeader(http.StatusCreated)
			fmt.Fprintf(w, "NamedBackup with %v Success", bk.Name)
		}
	}
}

func (ds *DictStore) wordHandler(w http.ResponseWriter, r *http.Request) {
	// path := strings.Split(r.URL.Path[:], "/")
	// fmt.Println("splited path: ", path, " len: ", len(path))
	switch r.Method {
	case "GET":
		log.Println("in getting GET of word")
		// path := r.URL.Path[1:3]
		path := strings.Split(r.URL.Path[:], "/")
		// token := path[2]
		// word := path[3]
		log.Println("get path: ", path, " len: ", len(path))
		// rUrl, _ := url.Parse(r.URL.Path[:])
		// log.Println("req url ", rUrl)
		qMap, _ := url.ParseQuery(r.URL.RawQuery)
		log.Println("query map: ", qMap)

		/*
			if query arg, filter with args
			elif not query arg, return all words

		*/
		var items []Word
		var onlyUnknown int
		var wordType []string

		if len(qMap) == 0 {
			token := "word"
			items = ds.SearchWord(token, nil)
			log.Println("found all items: ", len(items))

		} else if known, ok := qMap["Known"]; ok {
			if known[0] == "false" || known[0] == "False" || known[0] == "0" {
				onlyUnknown = 0
				log.Println("known=false")
			} else {
				onlyUnknown = 1
			}
			if recordType, ok := qMap["RecordType"]; ok {
				wordType = recordType
				items = ds.SearchWordByTypeWithKnown(wordType, onlyUnknown)
			} else {
				items = ds.AllWordWithKnown(onlyUnknown)
			}
			log.Println("found ", wordType, " known:", onlyUnknown, " items: ", len(items))
		} else if wordType, ok := qMap["RecordType"]; ok {
			items = ds.SearchWordByType(wordType)
			log.Println("found ", wordType, " items: ", len(items))
		}

		// if len(path) >= 2 && path[2] == "" {
		//
		//     token := "word"
		//     items = ds.SearchWord(token, nil)
		//     fmt.Println(items)
		// } else if len(path) > 2 && IsWordType(path[2]) {
		//     wordType := path[2]
		//     items = ds.SearchWordByType(wordType)
		//     log.Println(items)
		//
		// }
		headers := r.Header.Get("Accept")
		log.Println("header accept: ", headers)
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text")

		respJson := RespJSON{items, "pre", "next"}
		jsondata, err := json.Marshal(&respJson)
		if err != nil {
			log.Printf("json marshal error\n")
		}
		// fmt.Println(string(jsondata[:]))

		fmt.Fprintf(w, string(jsondata[:]))

	case "POST":
		log.Println("in getting POST of word")
		value, err := ioutil.ReadAll(r.Body)
		fmt.Println(string(value[:]))
		if err != nil {
			log.Println("error when read post body:", err)
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "error when read post body ")
		}

		var wd Word
		if err := json.Unmarshal(value, &wd); err != nil {
			log.Println("err when Unmarshal json")
		}
		log.Println("the posted data: ", wd)
		if err := ds.InsertWord(&wd); err != nil {
			log.Println("err when InsertWord")
			//TODO: handel different errors
			fmt.Fprintf(w, "InsertWord failed")
		} else {
			w.WriteHeader(http.StatusCreated)
			fmt.Fprintf(w, "InsertWord Success")
		}
	}
}

func (ds *DictStore) wordEditHandler(w http.ResponseWriter, r *http.Request) {
	// path := strings.Split(r.URL.Path[:], "/")
	// fmt.Println("splited path: ", path, " len: ", len(path))
	switch r.Method {
	case "PUT":
		vars := mux.Vars(r)
		wid := vars["wordId"]
		var msg string
		log.Println("in getting PUT of word")
		path := strings.Split(r.URL.Path[:], "/")
		log.Println("put path: ", path, " len: ", len(path))

		value, err := ioutil.ReadAll(r.Body)
		log.Println("put data: ", string(value[:]))
		if err != nil {
			log.Println("error when read post body:", err)
			w.WriteHeader(http.StatusBadRequest)
			// fmt.Fprintf(w, "error when read post body ")
			msg = "error when read post body, " + msg
		}

		var wd Word
		// var wdc WordComing
		if err := json.Unmarshal(value, &wd); err != nil {
			log.Println("err when Unmarshal json")
			// if err == UnmarshalTypeError && err.Value == "bool" {
			//
			//     if wd.Known == "true" || wd.Know == "True" {
			//         wd.Known = true
			//     }
			// }
			msg = "err when Unmarshal json" + msg
		} else {
			// wd := Word{}
			log.Println("put word: ", wd, "type of Known: ", reflect.TypeOf(wd.Known))

			wordId, err := strconv.ParseUint(wid, 10, 32)
			if err != nil {
				log.Println("err when Update, cannot part path[2] to uint as word id")
				// fmt.Fprintf(w, "Update failed, cannot part path[2] to uint as word id")
				msg = "Update failed, cannot part path[2] to uint as word id" + msg
			} else {
				if err := ds.Update(uint(wordId), &wd); err != nil {
					log.Println("err when Update")
					//TODO: handel different errors
					// fmt.Fprintf(w, "Update failed")
					msg = "Update failed!" + msg
				} else {
					w.WriteHeader(http.StatusOK)
					// fmt.Fprintf(w, "Update Success")
					msg = "Update Success!" + msg
				}
			}
		}
		// w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, msg)
	}
}

func JSONResp(w http.ResponseWriter, items []Word) string {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	respJson := RespJSON{items, "pre", "next"}
	jsondata, err := json.Marshal(&respJson)
	if err != nil {
		log.Printf("json marshal error\n")
	}
	return string(jsondata[:])
}

func (ds *DictStore) lookupHandler(w http.ResponseWriter, r *http.Request) {
	// path := strings.Split(r.URL.Path[:], "/")
	vars := mux.Vars(r)
	lan := vars["lan"]
	token := vars["token"]
	// log.Println("splited path: ", path, " len: ", len(path))
	switch r.Method {
	case "GET":
		// path := r.URL.Path[1:3]
		// token := path[2]
		// word := path[3]
		log.Println("token: ", token, "lan: ", lan)
		items := ds.searchWordByLan(lan, token)

		headers := r.Header.Get("Accept")
		log.Println("header accept: ", headers)

		// fmt.Println(string(jsondata[:]))

		// fmt.Fprintf(w, string(jsondata[:]))
		fmt.Fprintf(w, JSONResp(w, items))
	}
}

func (ds *DictStore) searchWordByLan(lan string, wd string) []Word {
	token := lan
	word := wd
	var items []Word
	// log.Println("path: ", path, "token: ", token, "word: ", word)
	if IsSearchToken(token) {
		log.Printf("ok, to get: %v from %v \n", word, token)
		items = ds.SearchWord(token, &word)
		log.Println("found words: ", len(items))
	}
	return items
}

type wordtypesJSON struct {
	Content []string
}

func (ds *DictStore) wordTypesHandler(w http.ResponseWriter, r *http.Request) {
	// path := strings.Split(r.URL.Path[:], "/")
	// fmt.Println("splited path: ", path, " len: ", len(path))
	switch r.Method {
	case "GET":
		// path := r.URL.Path[1:3]
		// token := path[2]
		// word := path[3]
		// log.Println("path: ", path, "token: ", token, "word: ", word)
		// if IsSearchToken(token) {
		// fmt.Printf("ok, to get: %v from %v \n", word, token)
		// items := ds.SearchWord(token, &word)
		items := WordTypes
		fmt.Println(items)
		headers := r.Header.Get("Accept")
		log.Println("header accept: ", headers)
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text")

		respJson := wordtypesJSON{items}
		jsondata, err := json.Marshal(&respJson)
		if err != nil {
			log.Printf("json marshal error\n")
		}
		// log.Println(string(jsondata[:]))

		fmt.Fprintf(w, string(jsondata[:]))
		// }
	}
}

// var DStore *DictStore
// var db *gorm.DB

// var openerr error

func runServer() {
	// RestoreDB()
	InitConfig()
	dStore, _ := OpenDB()
	fmt.Println("dStore: ", dStore)
	defer dStore.Close()

	dStore.Backup()
	// dStore.ImportCSV("csv_test.csv")
	db := dStore.db
	// dStore = &dStore
	// db, openerr = gorm.Open("sqlite3", "dict.db")
	// if openerr != nil {
	//     panic("failed to connect database")
	// }
	// defer db.Close()
	//
	// // migrate the schema
	// db.AutoMigrate(&Word{})
	// db.AutoMigrate(&MostBasicWord{})
	// db.SingularTable(false)

	// db.Create(&Word{
	//     Se: "ja",
	//     En: "yes",
	//     Cn: "是",
	//     // RecordType: "basic"
	// })

	var word Word
	db.First(&word, "se = ?", "aldrig")

	fmt.Printf("get most MostBasicWordword: %v\n", word.Se)

	// db.Create(&Word{
	//     Se: "nej",
	//     En: "no",
	//     Cn: "不"})

	var mbword Word
	// db.First(&mbword, "en = ?", "no")
	mbword = dStore.GetWordByPk(1)

	fmt.Printf("get word: %v\n", mbword.Se)

	// word.Cn = "thisiscnnever"
	// word.Se = "aldrigofse"
	// dStore.Update(&word)
	// var nword Word
	// db.First(&nword, "se Like ?", "%aldrig%")
	// fmt.Printf("updated word: %v", nword)

	// const HOST = "0.0.0.0:8080"
	// conf, _ := LoadConfig(CONFIG_FILE)
	conf := CONFIG
	HOST := conf.HostIP + ":" + conf.HostPort
	http.HandleFunc("/lookup/", dStore.lookupHandler)
	http.HandleFunc("/wordtypes", dStore.wordTypesHandler)
	http.HandleFunc("/words/", dStore.wordHandler)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.HandleFunc("/", viewHandler)
	log.Println("Listening on: ", HOST)
	log.Fatal(http.ListenAndServe(HOST, nil))
}

func runServerGorilla() {
	// RestoreDB()
	InitConfig()
	dStore, _ := OpenDB()
	fmt.Println("dStore: ", dStore)
	defer dStore.Close()

	dStore.Backup()
	// dStore.ImportCSV("csv_test.csv")
	// db := dStore.db
	// dStore = &dStore
	// db, openerr = gorm.Open("sqlite3", "dict.db")
	// if openerr != nil {
	//     panic("failed to connect database")
	// }
	// defer db.Close()
	//
	// // migrate the schema
	// db.AutoMigrate(&Word{})
	// db.AutoMigrate(&MostBasicWord{})
	// db.SingularTable(false)

	// const HOST = "0.0.0.0:8080"
	// conf, _ := LoadConfig(CONFIG_FILE)
	conf := CONFIG
	HOST := conf.HostIP + ":" + conf.HostPort

	r := mux.NewRouter()
	r.HandleFunc("/lookup/{lan}/{token}", dStore.lookupHandler).Methods("GET")
	r.HandleFunc("/wordtypes", dStore.wordTypesHandler).Methods("GET")
	r.HandleFunc("/words", dStore.wordHandler).Methods("GET", "POST")
	r.HandleFunc("/words/{wordId}", dStore.wordEditHandler).Methods("PUT")
	r.HandleFunc("/backups", dStore.backupHandler).Methods("GET", "POST")
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static")))).Methods("GET")
	r.HandleFunc("/management", managementPageHandler).Methods("GET")
	r.HandleFunc("/", frontPageHandler).Methods("GET")
	log.Println("Listening on: ", HOST)

	// log.Fatal(http.ListenAndServe(HOST, r))

	server := &http.Server{Handler: r}
	l, err := net.Listen("tcp4", HOST)
	if err != nil {
		log.Fatal(err)
	}
	err = server.Serve(l)
}

func init() {
	logger = log.New(os.Stdout, "[webapp]", log.Lshortfile|log.LstdFlags)
}

func main() {
	// args := os.Args
	// if len(args) == 1 {
	//     //local conf in default
	//
	// } else {
	//     if args[1] == "local" {
	//     } else if args[1] == "etcd" {
	//     }
	// }
	//

	runServerGorilla()
	// drivemain()
}

package main

import (
	"log"
	"os"
	"testing"
	// "github.com/jinzhu/gorm"
	// _ "github.com/jinzhu/gorm/dialects/sqlite"
)

//
// var db *gorm.DB
// var openerr error
//
// type TestDB struct {
//     db *gorm.DB
// }

func NewTestDB() *DictStore {
	InitConfigTest()
	// db, openerr := gorm.Open("sqlite3", "dict_test.db")
	ds, openerr := OpenDB()
	if openerr != nil {
		panic("failed to connect database")
	}
	// defer db.Close()

	db := ds.db
	// migrate the schema
	db.AutoMigrate(&Word{})
	return ds
}

func CleanUp(ds *DictStore) {
	defer os.Remove(CONFIG.DBFileName)
	db := ds.db
	db.Close()
}

func TestUpdate(t *testing.T) {
	ds := NewTestDB()
	db := ds.db
	defer CleanUp(ds)

	wd := Word{
		Se:         "ja",
		En:         "yes",
		Cn:         "是",
		RecordType: "basic"}
	if err := ds.InsertWord(&wd); err != nil {
		t.Errorf("error when InsertWord ")
	}

	var word Word
	db.First(&word, "se = ?", "ja")

	word.Cn = "确实是"
	word.Se = "thisisja"
	if err := ds.Update(word.ID, &word); err != nil {
		t.Errorf("error when dStore.Update")
	}
	var nword Word
	db.First(&nword, "En = ?", "yes")
	log.Printf("updated word: %v", nword)
	if nword.Cn != word.Cn || nword.Se != word.Se {
		t.Errorf("updated word not match, expected: %v, got: %v,", word, nword)
	}

}
func TestInsertWord(t *testing.T) {
	ds := NewTestDB()
	defer CleanUp(ds)
	// db := ds.db
	// db, openerr = gorm.Open("sqlite3", "dict_test.db")
	// if openerr != nil {
	//     panic("failed to connect database")
	// }
	// defer db.Close()
	//
	// // migrate the schema
	// db.AutoMigrate(&Word{})
	// db.AutoMigrate(&MostBasicWord{})
	// db.SingularTable(false)

	// db.Create(&Word{
	//     Se: "ja",
	//     En: "yes",
	//     Cn: "是",
	//     // RecordType: "basic"
	// })

	// var word Word
	// db.First(&word, "se = ?", "ja")
	wd := Word{
		Se:         "ja",
		En:         "yes",
		Cn:         "是",
		RecordType: "basic"}
	if err := ds.InsertWord(&wd); err != nil {
		t.Errorf("error when InsertWord ")
	}
	if err := ds.InsertWord(&wd); err == nil {
		t.Errorf("error when InsertWord ")
	}
}

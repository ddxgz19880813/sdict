FROM golang:1.8

ADD . /go/src/gitlab.com/ddxgz/sdict

RUN go get gitlab.com/ddxgz/sdict

RUN go install gitlab.com/ddxgz/sdict

WORKDIR /go/src/gitlab.com/ddxgz/sdict

ENTRYPOINT /go/bin/sdict

EXPOSE 8080

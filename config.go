package main

import (
	"encoding/json"
	"fmt"
	"log"
	// "time"
	// "os"
	// "context"
	"io/ioutil"
	// "github.com/coreos/etcd/client"
)

const (
	CONFIG_FILE      string = "conf.json"
	CONFIG_FILE_TEST string = "conf_test.json"
)

type Config struct {
	DBType      string
	DBPath      string
	DBFileName  string
	HostIP      string
	HostPort    string
	StaticPath  string
	EtcdIP      string
	RecordTypes []string
}

var CONFIG Config

//
// func EtcdConfig(etcdIP string) {
//     cfg := client.Config{
//         Endpoints:               []string{etcdIP},
//         Transport:               client.DefaultTransport,
//         HeaderTimeoutPerRequest: time.Second,
//     }
//     c, err := client.New(cfg)
//     if err != nil {
//         log.Fatal(err)
//     }
//     kapi := client.NewKeysAPI(c)
//
// }

func InitConfig() {
	LoadConfig(CONFIG_FILE, &CONFIG)
}

func InitConfigTest() {
	LoadConfig(CONFIG_FILE_TEST, &CONFIG)
}

func LoadConfig(filename string, conf *Config) error {
	fmt.Println("In LoadConfig")

	file, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println("Error when opening conf.json: ", err)
	}

	// var conf Config
	err = json.Unmarshal(file, &conf)
	if err != nil {
		log.Println("Unmarshal err: ", err)
	}
	// fmt.Println("Unmarshaled: ", m)

	// decoder := json.NewDecoder(file)
	// conf := Config{}
	// err = decoder.Decode(&conf)
	// if err != nil {
	//     log.Println("Error when reading config file")
	// }
	log.Println("conf: ", conf)
	return nil
}
